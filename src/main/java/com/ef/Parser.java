package com.ef;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.ef.config.ApplicationContextProvider;
import com.ef.domain.LogAccess;
import com.ef.service.LogAccessService;

@SpringBootApplication
public class Parser {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Parser.class);
	
	private final static int ZERO = 0;
	private final static String IP = "192.168.182.114";

	public static void main(String[] args) {
		SpringApplication.run(Parser.class, args);
		
		Map<String, String> arguments = validate(args);
		if(arguments != null && arguments.size() > ZERO) {
			LogAccessService logAccessService = ApplicationContextProvider.getApplicationContext().getBean(LogAccessService.class);
			logAccessService.deleteAll();
			logAccessService.importData();
			List<String> ips = logAccessService.search(arguments);
			
			ips.forEach(ip -> {
				LOGGER.info("Find ip: {}", ip);
			});
			
			List<LogAccess> listLogAccess = logAccessService.findByIp(IP);
			
			listLogAccess.forEach(lo -> {
				LOGGER.info("Request made by ip: {} : {}", IP, lo.toString());
			});
		}
		LOGGER.info("### Done");
	}

	private static Map<String, String> validate(String[] args) {
		Map<String, String> arguments = new HashMap<>();
		if(args.length != 3) {
			LOGGER.error("Invalid Arguments");
		}else {
			for(int i =0; i < args.length; i++) {
				String params[] = args[i].split("=");
				LOGGER.info("args[" + i + "]: " + args[i]);
				arguments.put(params[0].substring(2), params[1]);
			}
		}
		return arguments;
	}
}
