package com.ef.repository;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.ef.domain.LogAccess;

public interface LogAccessRepository extends CrudRepository<LogAccess, Long>{

	@Query("SELECT ip FROM LogAccess la where la.requestAt between ?1 and ?2 group by la.ip having count(la.ip) > (?3)")
	Collection<String> findByRequestAtBetweenGroupByIpHavingCountIp(LocalDateTime initialDate, LocalDateTime finalDate, Long threshold);
	
	List<LogAccess> findByIp(String ip);
}
