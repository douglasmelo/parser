package com.ef.utils;

import org.apache.commons.lang3.builder.ToStringStyle;

public class ToStringBuilder {

	public static String toString(Object object) {
		return org.apache.commons.lang3.builder.ToStringBuilder.reflectionToString(object, ToStringStyle.JSON_STYLE);
	}
}
