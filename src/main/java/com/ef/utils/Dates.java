package com.ef.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Dates {

	public static String getFormattedDate(LocalDateTime localDateTime, String pattern) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
		return localDateTime.format(formatter);
	}
	
	public static LocalDateTime getLocalDateTime(String date, String pattern) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
		return LocalDateTime.parse(date, formatter);
	}
}