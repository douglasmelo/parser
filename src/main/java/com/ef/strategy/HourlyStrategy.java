package com.ef.strategy;

import java.time.LocalDateTime;

import org.springframework.stereotype.Component;

@Component
public class HourlyStrategy implements DurationStrategy {

	private static final long ONE = 1;
	
	@Override
	public LocalDateTime getFinalDate(LocalDateTime initialDate) {
		return initialDate.plusHours(ONE);
	}


}
