package com.ef.strategy;

import java.time.LocalDateTime;

public interface DurationStrategy {

	LocalDateTime getFinalDate(LocalDateTime initialDate);

}
