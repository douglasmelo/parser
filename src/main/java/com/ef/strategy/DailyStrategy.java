package com.ef.strategy;

import java.time.LocalDateTime;

import org.springframework.stereotype.Component;

@Component
public class DailyStrategy implements DurationStrategy {

	private static final long ZERO = 0;
	private static final long ONE = 1;
	private static final long TWENTY_THREE = 23;
	private static final long FIFTY_NINE = 59;
	
	
	@Override
	public LocalDateTime getFinalDate(LocalDateTime initialDate) {
		if(initialDate.getHour() == ZERO && initialDate.getMinute() == ZERO && initialDate.getSecond() == ZERO) {
			return initialDate.plusHours(TWENTY_THREE)
			.plusMinutes(FIFTY_NINE)
			.plusSeconds(FIFTY_NINE);
		}
		return initialDate.plusDays(ONE);
	}

}
