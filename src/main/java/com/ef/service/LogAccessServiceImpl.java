package com.ef.service;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ef.domain.Duration;
import com.ef.domain.LogAccess;
import com.ef.factory.DurationStrategyFactory;
import com.ef.repository.LogAccessRepository;
import com.ef.strategy.DurationStrategy;
import com.ef.utils.DatePatterns;
import com.ef.utils.Dates;

@Service
public class LogAccessServiceImpl implements LogAccessService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(LogAccessServiceImpl.class);
	
	private final static String WEB_SERVER_ACCESS_LOG = "web.server.access.log";
	private final static String BULK_INSERT_QUANTITY = "bulk.insert.quantity";
	private final static String IMPORT_DATA = "import.data";
	private final static String START_DATE = "startDate";
	private final static String DURATION = "duration";
	private final static String THRESHOLD = "threshold";
	
	
	@Autowired private Environment env;
	@Autowired LogAccessRepository logAccessRepository;
	
	@Override
	public LogAccess save(LogAccess log) {
		return this.logAccessRepository.save(log);
	}
	
	@Override
	public List<LogAccess> findByIp(String ip){
		return this.logAccessRepository.findByIp(ip);
	}

	@Override
	@Transactional(rollbackFor = Throwable.class)
	public List<String> search(Map<String, String> arguments) {
		LocalDateTime initialDate = Dates.getLocalDateTime(arguments.get(START_DATE).replaceAll("\\.", " "), DatePatterns.YYYY_MM_DD_HH_MM_SS);
		DurationStrategy strategy = DurationStrategyFactory.getInstance().getStrategy(Duration.valueOf(arguments.get(DURATION).toUpperCase()));
		LocalDateTime finalDate = strategy.getFinalDate(initialDate);
		
		LOGGER.info("Start search for data");
		Collection<String> ips = this.logAccessRepository.findByRequestAtBetweenGroupByIpHavingCountIp(initialDate, finalDate, new Long(arguments.get(THRESHOLD)));
		LOGGER.info("Finish search for data");
		
		return new ArrayList<String>(ips);
	}

	@Override
	public void deleteAll() {
		LOGGER.info("Start delete data");
		
		if(env.getProperty(IMPORT_DATA, Boolean.class)) {
			this.logAccessRepository.deleteAll();
		}else {
			LOGGER.info("Configured for not delete data, look at application property file");
		}
		
		LOGGER.info("Finish delete data");
	}

	@Override
	public void importData() {
		LOGGER.info("Start importing data");
		if(env.getProperty(IMPORT_DATA, Boolean.class)) {
			try {
				File file = new File(env.getProperty(WEB_SERVER_ACCESS_LOG));
				List<String> lines = FileUtils.readLines(file);
				List<LogAccess> listLogs = new ArrayList<>();
				for (String line : lines) {
					String splitLine[] = line.split("\\|");
					LocalDateTime requestAt = Dates.getLocalDateTime(splitLine[0], DatePatterns.YYYY_MM_DD_HH_MM_SS_SSS);
					LogAccess log = new LogAccess(requestAt, splitLine[1], splitLine[2], splitLine[3], splitLine[4]);
					if(listLogs.size() <= env.getProperty(BULK_INSERT_QUANTITY,Integer.class)) {
						listLogs.add(log);
					}else {
						logAccessRepository.save(listLogs);
						listLogs = new ArrayList<>();
						listLogs.add(log);
					}
				}
				logAccessRepository.save(listLogs);
				LOGGER.info("Finish importing data");
			} catch (IOException e) {
				System.out.println("Unable to read file");
				LOGGER.error(e.getMessage(),e);
			}
		}else {
			LOGGER.info("Configured for not import data, look at application property file");
		}
		
	}
}
