package com.ef.service;

import java.util.List;
import java.util.Map;

import com.ef.domain.LogAccess;

public interface LogAccessService {

	LogAccess save(LogAccess log);
	
	List<String> search(Map<String, String> arguments);

	void deleteAll();

	void importData();

	List<LogAccess> findByIp(String ip);

}
