package com.ef.factory;

import com.ef.config.ApplicationContextProvider;
import com.ef.domain.Duration;
import com.ef.strategy.DurationStrategy;

public class DurationStrategyFactory {

	public static DurationStrategyFactory getInstance() {
		return new DurationStrategyFactory();
	}
	
	public DurationStrategy getStrategy(Duration duration) {
		
		DurationStrategy strategy = (DurationStrategy) 
				ApplicationContextProvider.getApplicationContext().getBean(duration.getStrategy());
		
		return strategy;
	}
}
