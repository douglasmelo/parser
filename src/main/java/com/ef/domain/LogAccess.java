package com.ef.domain;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "log_access")
public class LogAccess extends DomainModel<LogAccess> {

	private static final long serialVersionUID = 3112769047811984610L;
	
	@Column(columnDefinition = "datetime", nullable = false)
	private LocalDateTime requestAt;
	
	@NotEmpty
	@Column(nullable = false)
	private String ip;
	
	@NotEmpty
	@Column(nullable=false)
	private String logRequest;
	
	@Column(nullable=false)
	private String status;
	
	@Column(nullable=false)
	private String userAgent;
	
	public LogAccess() {
		
	}
	
	public LogAccess(LocalDateTime requestAt, String ip, String logRequest, String status, String userAgent) {
		this.requestAt = requestAt;
		this.ip = ip;
		this.logRequest = logRequest;
		this.status = status;
		this.userAgent = userAgent;
	}

}
