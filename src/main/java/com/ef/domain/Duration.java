package com.ef.domain;

import com.ef.strategy.DailyStrategy;
import com.ef.strategy.DurationStrategy;
import com.ef.strategy.HourlyStrategy;

public enum Duration {

	HOURLY("hourly", HourlyStrategy.class),
	DAILY("daily", DailyStrategy.class);
	
	private String description;
	private Class<? extends DurationStrategy> strategy;
	
	private Duration(String description, Class<? extends DurationStrategy> strategy) {
		this.description = description;
		this.strategy = strategy;
	}

	public Class<? extends DurationStrategy> getStrategy() {
		return strategy;
	}

	public String getDescription() {
		return description;
	}
}
