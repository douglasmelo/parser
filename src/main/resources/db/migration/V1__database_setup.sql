CREATE TABLE `log_access` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `request_at` datetime NOT NULL,
  `ip` varchar(15) NOT NULL,
  `log_request` varchar(255) NOT NULL,
  `status` varchar(25) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8; 