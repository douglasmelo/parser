# PARSER
This application is responsible for import log and search on this log.

### Stack
- [java 8](http://www.oracle.com/technetwork/java/javase/overview/java8-2100321.html) - Programming language
- [spring-boot 1.5.10.RELEASE](https://github.com/spring-projects/spring-boot) - Provide application services
- [mysql 5.1.45](https://dev.mysql.com/downloads/connector/j) - Mysql Connector Java

**Building in a local environment:**
- Clone the code from the repository:
- Open the terminal go to some folder and type

```
git clone git@bitbucket.org:douglasmelo/parser.git
```

- Configure the application properties according to your necessity, database configuration, web server log file to load, quantity for bulk insert and also if you desire to always import the file to database:

``` 
web.server.access.log=/home/apps/parser/access.log

bulk.insert.quantity=20000

import.data=true
```

- Open the terminal
- Go to the folder that the code was cloned and type

```
./mvnw clean package
```

**Starting the application:**
- Create a database parser
- Go to the folder that the code was cloned and type 

- Clone the code from the repository:
- Open the terminal go to some folder and type

```
cd target
java -jar parser-0.0.1-SNAPSHOT.jar --startDate=2017-01-01.13:00:00 --duration=hourly --threshold=100
or
java -jar parser-0.0.1-SNAPSHOT.jar --startDate=2017-01-01.13:00:00 --duration=daily --threshold=250
or
java -jar parser-0.0.1-SNAPSHOT.jar --startDate=2017-01-01.00:00:00 --duration=daily --threshold=500

```


### SQL Queries for SQL test

```
select ip, count(*) from log_access where request_at between '2017-01-01.13:00:00' and '2017-01-01.14:00:00' group by ip having count(ip) > 100;

select ip, count(*) from log_access where request_at between '2017-01-01.00:00:00' and '2017-01-01.23:59:59' group by ip having count(ip) > 500;
```
